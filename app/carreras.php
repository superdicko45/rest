<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class carreras extends Model
{
    protected $table = 'carreras';

    protected $fillable = [
        'nom_carrera', 'jefe_carrera', 'presentacion','nom_plan_estudio','siglas','titulo','id_unidad_academica','id_area_conocimiento', 'id_modalidad', 'num_periodos', 'tel_contacto', 'id_status', 'num_sep', 'perfil_ingreso', 'perfil_egreso', 'requisitos_ing', 'requisitos_egr', 'nivel', 'bandera'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function alumno()
    {
        /* Se referencia a la tabla alumno por su id_carrera */
        return $this->belongsTo('App\alumnos','id_carrera');
    }

}
