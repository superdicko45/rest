<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class alumnos extends Model
{
    protected $table = 'alumnos';

    protected $fillable = [
        'matricula',
		'nombre',
		'ape_paterno',
		'ape_materno',
		'sexo',
		'curp',
		'direccion',
		'email',
		'tel_movil',
		'tel_tutor',
		'fecha_nac',
		'fecha_ingreso',
		'grado',
		'id_cat_status',
		'id_carrera',
		'turno',
		'tutor',
		'edad',
		'foto_perfil',
		'status',
		'bandera'	
    ];

    protected $hidden = ['created_at', 'updated_at', 'password', 'remember_token'];

    protected $primaryKey = 'matricula';

    public function carrera()
    {
        /* Se referencia a la tabla carrera por su id */
        return $this->hasOne('App\carreras','id_carrera','id_carrera');
    }

}
