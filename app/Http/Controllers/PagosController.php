<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\DB;


class PagosController extends Controller
{



	public function getPagos(Request $request)
	{   
		
		$matricula = $request->input('matricula');

		$alumno = DB::table('alumnos')
						->select('id_carrera')
						->where('matricula', $matricula)
						->get();

		
		if($alumno->isEmpty()){
			return response()->json(['error'=>true, 'status' => 'No se encontro matricula'], 301);
		}else{

            $data = DB::table('matriculas')
                        ->join('alumnos', 'matriculas.id_alumno', '=', 'alumnos.matricula')
                        ->join('carreras', 'alumnos.id_carrera', '=', 'carreras.id_carrera')
                        ->join('ciclos', 'matriculas.id_ciclo', '=', 'ciclos.id_ciclo')
                        ->select(
                            DB::raw('LPAD(matriculas.id_matricula, 6, "0") AS folio'),
                            'ciclos.ciclo as ciclo',
                            DB::raw('CONCAT(alumnos.nombre, " ",alumnos.ape_paterno, " ", alumnos.ape_materno) as nombre'),     
                            'alumnos.matricula',
                            'carreras.nom_carrera as carrera',
                            'matriculas.grado',
                            'matriculas.referencia',
                            'matriculas.updated_at as fecha'
                            )
                        ->where('matriculas.id_alumno', $matricula)
                        ->get();

			return response()->json(['error'=>false, 'data' => $data], 200);
		}
		
	}

}   