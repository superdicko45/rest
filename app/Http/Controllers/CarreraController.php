<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\DB;

class CarreraController extends Controller
{

    public function getInfo(Request $request)
    {   

        $data = DB::table('carreras')
            ->join('cat_modalidad', 'carreras.id_modalidad', '=', 'cat_modalidad.id')
            ->join('cat_nom_periodo', 'carreras.id_nom_periodo', '=', 'cat_nom_periodo.id')   
            ->select(
                'carreras.id_carrera',
                'carreras.nom_carrera',
                'carreras.presentacion',
                'carreras.jefe_carrera',
                'carreras.nom_plan_estudio',
                'carreras.titulo',
                'carreras.id_unidad_academica',
                'carreras.id_area_conocimiento',
                'cat_modalidad.modalidad',
                'cat_nom_periodo.nombre as periodos',
                'carreras.num_periodos',
                'carreras.tel_contacto',
                'carreras.num_sep',
                'carreras.nivel',
                'carreras.perfil_ingreso',
                'carreras.perfil_egreso',
                'carreras.requisitos_ing',
                'carreras.requisitos_egr'
            )
            ->where('bandera',1)
            ->where('id_carrera',$request->input('id_carrera'))
            ->get();

        if($data->isEmpty()) return response()->json(['error'=>true, 'data' => 'Sin coincidencias'], 200);

        else return response()->json(['error'=>false, 'data' => $data], 200);
    }

    public function getPlan(Request $request)
    {   

        $data = DB::table('carreras')
            ->join('plan_materias', 'carreras.id_carrera', '=', 'plan_materias.id_carrera')
            ->join('materias', 'plan_materias.id_materia', '=', 'materias.id_materia')   
            ->select(
                'materias.nom_materia',
                'plan_materias.grado',
                'plan_materias.id_materia',
                'plan_materias.creditos'
            )
            ->where('carreras.bandera',1)
            ->where('plan_materias.id_carrera',$request->input('id_carrera'))
            ->orderBy('plan_materias.grado', 'asc')
            ->get();

        if($data->isEmpty()) return response()->json(['error'=>true, 'data' => 'Sin coincidencias'], 200);

        else return response()->json(['error'=>false, 'data' => $data], 200);
    }

    public function getCarreras(Request $request){   

        if($request->input('tipo') == 'mae'){
            $data = DB::table('carreras')
                ->where('nivel', '=', 'Maestría')
                ->where('bandera',1)
                ->select('id_carrera', 'nom_carrera', 'presentacion')
                ->get();

        }elseif($request->input('tipo') == 'lic'){
            $data = DB::table('carreras')
                ->where('nivel', '=', 'Licenciatura')
                ->select('id_carrera', 'nom_carrera', 'presentacion')
                ->where('bandera',1)
                ->get();

        }else{
            return response()->json(['error'=>true, 'data' => 'Sin coincidencias'], 200);
        }
        
        $data->map(function($item, $key){
            
            if($key % 2 == 0) $item->imagen_carrera = 'http://ceu.edu.mx/wp-content/uploads/2015/06/carreras-thumb2.jpg';
            else $item->imagen_carrera = 'http://ceu.edu.mx/wp-content/uploads/2015/06/index-thumb-02.jpg';

            return $item;
        });     

        return response()->json(['error'=>false, 'data' => $data], 200);
    }
    
}
