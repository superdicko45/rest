<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use JWTFactory;
use JWTAuth;
use App\Alumno;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password'=> 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' =>true, 'data' => 'credenciales invalidas'], 200);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => true, 'data'=>'could_not_create_token'], 300);
        }

        $alumno = Auth::user();
        $response['error'] = false;
        $response['data']['user']['token'] = $token;
        $response['data']['user']['matricula'] = $alumno->matricula;
        $response['data']['user']['nombre'] = $alumno->nombre.' '.$alumno->ape_paterno;
        //$response['data']['user']['foto_perfil'] = url('/'.$alumno->foto_perfil);
        $response['data']['user']['foto_perfil'] = url('http://10.0.2.2/uploads/avatars_us/'.$alumno->foto_perfil);

        return response()->json($response);
    }
}
