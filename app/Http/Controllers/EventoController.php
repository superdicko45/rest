<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\DB;


class EventoController extends Controller
{

	public function getEventos(Request $request)
	{   
		
        $data = DB::table('eventos')
                    ->where('bandera', 1)
                    ->orderBy('updated_at', 'desc')
                    ->limit(20)
                    ->get();

        $data->map(function($item, $key){
        	$item->imagen_evento = 'http://systemedu.consultores-profesionales-en-educacion.com/public//uploads/avatars_ev/no_image.jpg';
        	$item->lugar = 'Patio civico';

        	return $item;
        });           

		return response()->json(['error'=>false, 'data' => $data], 200);
		
	}

}   