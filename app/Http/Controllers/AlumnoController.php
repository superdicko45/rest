<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

use App\alumnos;
use App\carreras;
use Illuminate\Support\Facades\DB;


class AlumnoController extends Controller
{

	/**
	 * Show the application dashboard.
	 *
	 * @return Response
	 */

	public function index(Request $request)
	{   
		
		$matricula = $request->input('matricula');

		$alumno = DB::table('alumnos')
						->join('carreras', 'alumnos.id_carrera', '=', 'carreras.id_carrera')
						->select( 
							DB::raw('CONCAT(nombre, " ",ape_paterno, " ", ape_materno) as nombre'), 
							'carreras.nom_carrera as carrera',
							'grado',
							'fecha_ingreso as ingreso',
							'sexo',
							'curp',
							'tel_movil as movil',
							'fecha_nac as nacimiento',
							'edad', 
							'direccion',
							'email',
							'tutor',
							'id_cat_status as status'
						)
						->where('alumnos.matricula', $matricula)
						->get();

		
		if($alumno->isEmpty()){
			return response()->json(['error'=>true, 'status' => 'No se encontro matricula'], 301);
		}else{
			return response()->json(['error'=>false, 'data' =>$alumno], 200);
		}				

		return $alumno;

	}


	public function getHistorial(Request $request)
	{   
		
		$matricula = $request->input('matricula');

		$alumno = DB::table('alumnos')
						->select('id_carrera')
						->where('matricula', $matricula)
						->get();

		
		if($alumno->isEmpty()){
			return response()->json(['error'=>true, 'status' => 'No se encontro matricula'], 301);
		}else{
			return response()->json(['error'=>false, 'data' =>$this->historial($matricula, $alumno[0]->id_carrera)], 200);
		}
		
	}

	public function getObservaciones(Request $request){

		$matricula = $request->input('matricula');

		$alumno = DB::table('alumnos')
						->where('matricula', $matricula)
						->get();

		if($alumno->isEmpty()){
			return response()->json(['error'=>true, 'status' => 'No se encontro matricula'], 301);
		}else{
			return response()->json([
				'error'=>false, 
				'data' => DB::table('observaciones')
					->select('id_observacion', 'observaciones', 'updated_at')
					->where('id_alumno', $matricula)
					->get()], 
				200);
		}
	}

	public function historial($id, $id_carrera){
        
        $plan_materias=DB::table('plan_materias')->select('id_materia','creditos', 'grado')->where('id_carrera',$id_carrera)->get();
        $calificaciones = [];

        foreach ($plan_materias as $key => $value) {
            $ext=DB::table('extraordinarios')
                ->join('examenes', 'extraordinarios.id_extraordinario', '=', 'examenes.id_extraordinario')
                ->join('materias', 'extraordinarios.id_materia', '=', 'materias.id_materia')
                //->join('plan_materias', 'materias.id_materia', '=', 'plan_materias.id_materia')
                ->join('ciclos', 'extraordinarios.id_ciclo', '=', 'ciclos.id_ciclo') 
                ->select('materias.nom_materia', 'examenes.calificacion')  
                //->select('plan_materias.creditos','plan_materias.grado','materias.nom_materia','ciclos.ciclo','alum_grupo.calificacion')
                ->where('examenes.id_alumno','=',$id)
                ->where('extraordinarios.id_materia','=',$value->id_materia)
                ->orderBy('examenes.calificacion', 'asc')
                //->limit(1)
                ->get(); 

            //array_push($extraordinarios, $ext);    
        
            $ord=DB::table('grupo')
                ->join('alum_grupo', 'grupo.id_grupo', '=', 'alum_grupo.id_grupo')
                ->join('materias', 'grupo.id_materia', '=', 'materias.id_materia')
                //->join('plan_materias', 'materias.id_materia', '=', 'plan_materias.id_materia')
                ->join('ciclos', 'grupo.id_ciclo', '=', 'ciclos.id_ciclo') 
                ->select('materias.nom_materia', 'alum_grupo.calificacion')  
                //->select('plan_materias.creditos','plan_materias.grado','materias.nom_materia','ciclos.ciclo','alum_grupo.calificacion')
                ->where('alum_grupo.id_alumno','=',$id)
                ->where('grupo.id_materia','=',$value->id_materia)
                ->orderBy('alum_grupo.calificacion', 'asc')
                //->limit(1)
                ->get();  

            //array_push($extraordinarios, $ord);

            //dd()     
       
            if($ord->isEmpty() and $ext->isEmpty()){
                $plan_materias->forget($key);
            }

            elseif($ext->isEmpty()){
                $plan_materias[$key]->nom_materia=$ord['0']->nom_materia;
                $plan_materias[$key]->calificacion=$ord['0']->calificacion;
                $plan_materias[$key]->tipo='Ord';
                $plan_materias[$key]->ext=0;
                $plan_materias[$key]->ord=$ord->count();
                array_push($calificaciones, [
                	'materia' => $ord['0']->nom_materia,
                	'grado' => $plan_materias[$key]->grado,
                	'calificacion' => $ord['0']->calificacion 
                ]);
            }
            elseif($ord->isEmpty()){
                $plan_materias[$key]->nom_materia=$ext['0']->nom_materia;
                $plan_materias[$key]->calificacion=$ext['0']->calificacion;
                $plan_materias[$key]->tipo='Ext';
                $plan_materias[$key]->ext=$ext->count();
                $plan_materias[$key]->ord=0;
                array_push($calificaciones, [
                	'materia' => $ext['0']->nom_materia,
                	'grado' => $plan_materias[$key]->grado,
                	'calificacion' => $ext['0']->calificacion 
                ]);
            }
            else{
                if($ord['0']->calificacion > $ext['0']->calificacion){
                    $plan_materias[$key]->nom_materia=$ord['0']->nom_materia;
                    $plan_materias[$key]->calificacion=$ord['0']->calificacion;
                    $plan_materias[$key]->tipo='Ord';
                    $plan_materias[$key]->ext=$ext->count();
                    $plan_materias[$key]->ord=$ord->count();
                    array_push($calificaciones, [
	                	'materia' => $ord['0']->nom_materia,
	                	'grado' => $plan_materias[$key]->grado,
	                	'calificacion' => $ord['0']->calificacion 
	                ]);
                }else{
                    $plan_materias[$key]->nom_materia=$ext['0']->nom_materia;
                    $plan_materias[$key]->calificacion=$ext['0']->calificacion;
                    $plan_materias[$key]->tipo='Ext';
                    $plan_materias[$key]->ext=$ext->count();
                    $plan_materias[$key]->ord=$ord->count();
                    array_push($calificaciones, [
	                	'materia' => $ext['0']->nom_materia,
	                	'grado' => $plan_materias[$key]->grado,
	                	'calificacion' => $ext['0']->calificacion 
	                ]);
                }
            }                 
        }  

        //dd($plan_materias, $extraordinarios);
        $json['creditos']=DB::table('plan_materias')
            ->where('id_carrera', $id_carrera)
            ->sum('creditos');
          
        $json['promedio']=round($plan_materias->avg('calificacion'),2); 

        $json['creditos_act']=$plan_materias->sum('creditos');

        $json['porcentaje']=round(($json['creditos_act']/$json['creditos'])*100, 2);   

        $json['historial']=$calificaciones;
        //dd($json);
        return $json;
    }

}   