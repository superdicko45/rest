<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('user/login', 'Authenticate@login');

Route::middleware(['jwt.own'])->group(	function(){
	Route::post('user/index', 'AlumnoController@index')->name('getAlu');
	Route::post('user/histo', 'AlumnoController@getHistorial')->name('getHis');
	Route::post('user/obser', 'AlumnoController@getObservaciones')->name('getObs');

	Route::post('user/pagos', 'PagosController@getPagos')->name('getPag');

});

Route::get('user/evens', 'EventoController@getEventos')->name('getEve');

Route::post('user/lics', 'CarreraController@getCarreras')->name('getCar');
Route::post('user/lics/info', 'CarreraController@getInfo')->name('getICa');
Route::post('user/lics/plan', 'CarreraController@getPlan')->name('getPla');